package com.example.jintarkop;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MyReadDataService extends Service {
    DatabaseReference reffdb;
    MainActivity mainActivity;

    public MyReadDataService() {
        mainActivity = new MainActivity();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        reffdb = FirebaseDatabase.getInstance().getReference();
        reffdb.child("Pengering1").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChild("Timbangan1")) {
                    String Timbangan1 = dataSnapshot.child("Timbangan1").getValue().toString();
                    String Timbangan2 = dataSnapshot.child("Timbangan2").getValue().toString();
                    String berat1 = dataSnapshot.child("beratKopiKering1").getValue().toString();
                    String berat2 = dataSnapshot.child("beratKopiKering2").getValue().toString();

                    int a = Integer.parseInt(Timbangan1);
                    int b = Integer.parseInt(berat1);
                    int hasil;
                    if (a > b) {
                        MyFirebaseService.showNotification(getApplicationContext(), "Pemberitahuan", "Kopi Kering");
                    }


//                    int as = Integer.parseInt ( Timbangan2 );
//                    int bs = Integer.parseInt ( berat2 );
//                    int hasils;
//                    if ( as > bs){
//                        hasils = getPersenAir(as,bs);
//                        progressBar2.setMax ( 100 );
//                        progressBar2.setProgress ( hasils );
//                    } else {
//                        progressBar2.setMax ( 100 );
//                        progressBar2.setProgress ( 100 );
//                    }


//                    if (Integer.parseInt ( berat2 ) < Integer.parseInt ( Timbangan2 )){
//                        MyFirebaseService.showNotification(getActivity (), "Pemberitahuan", "Kopi Kering");
//                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        return START_STICKY;
        // return super.onStartCommand(intent, flags, startId);
    }
}
