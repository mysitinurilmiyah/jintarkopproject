package com.example.jintarkop;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_main );

        BottomNavigationView navigationView = findViewById ( R.id.bot_nav );

        final HomeFragment homeFragment = new HomeFragment ();
        final MonitorFragment monitorFragment = new MonitorFragment ();
        final NotifFragment notifFragment = new NotifFragment ();

        navigationView.setOnNavigationItemSelectedListener ( new BottomNavigationView.OnNavigationItemSelectedListener () {
            @Override
            public boolean onNavigationItemSelected ( @NonNull MenuItem menuItem ) {
                int id = menuItem.getItemId ();
                if (id == R.id.menu_home){
                    setFragment ( homeFragment );
                    return true;
                }else if (id == R.id.menu_monitoring){
                    setFragment ( monitorFragment );
                    return true;
                }
                return false;
            }
        } );

        navigationView.setSelectedItemId ( R.id.menu_home );

    }

    private void setFragment( Fragment fragment ){
        FragmentTransaction fragmentTransaction = getSupportFragmentManager ().beginTransaction ();
        fragmentTransaction.replace ( R.id.frame, fragment );
        fragmentTransaction.commit ();
    }
}
