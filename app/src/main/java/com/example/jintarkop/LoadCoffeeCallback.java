package com.example.jintarkop;

import com.example.jintarkop.pjo.Coffee;

import java.util.ArrayList;

interface LoadCoffeeCallback {
    void preExecute();
    void postExecute(ArrayList<Coffee> coffees);
}
