package com.example.jintarkop.db;

import android.net.Uri;
import android.provider.BaseColumns;

public class DatabaseContract {
    public static final String AUTHORITY = "com.example.jintarkop.db";
    private static final String SCHEME = "content";

    public static String TABLE_NAME = "category";
    public static final class CategoryColumns implements BaseColumns {
        public static String COFFEE_NAME = "name";
        public static String DRY_WEIGHT = "dry_weight";
    }

    public static final Uri CONTENT_URI = new Uri.Builder().scheme(SCHEME)
            .authority(AUTHORITY)
            .appendPath(TABLE_NAME)
            .build();
}
