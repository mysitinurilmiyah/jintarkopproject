package com.example.jintarkop.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.jintarkop.R;
import com.example.jintarkop.pjo.Coffee;

import java.util.ArrayList;

public class CoffeeAdapter extends RecyclerView.Adapter<CoffeeAdapter.CoffeeViewHolder> {
    private ArrayList<Coffee> coffees = new ArrayList<>();

    public void setCoffees(ArrayList<Coffee> coffees) {
        this.coffees = coffees;
    }

    @NonNull
    @Override
    public CoffeeViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_category_coffee_item,viewGroup,false);
        return new CoffeeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CoffeeViewHolder coffeeViewHolder, int i) {
        Coffee data = coffees.get(i);
        coffeeViewHolder.id.setText(data.getId());
        coffeeViewHolder.name.setText(data.getName());
        coffeeViewHolder.weight.setText(data.getWeight());
    }

    @Override
    public int getItemCount() {
        return coffees.size();
    }

    public class CoffeeViewHolder extends RecyclerView.ViewHolder {
        TextView id,name,weight;
        public CoffeeViewHolder(@NonNull View itemView) {
            super(itemView);
            id = itemView.findViewById(R.id.textView17);
            name = itemView.findViewById(R.id.textView16);
            weight = itemView.findViewById(R.id.textView15);
        }
    }
}
