package com.example.jintarkop;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.jintarkop.adapter.CoffeeAdapter;
import com.example.jintarkop.db.DatabaseContract;
import com.example.jintarkop.helper.MappingHelper;
import com.example.jintarkop.pjo.Coffee;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import static com.example.jintarkop.db.DatabaseContract.CONTENT_URI;

public class ListResourceActivity extends AppCompatActivity implements LoadCoffeeCallback, View.OnClickListener {

    private RecyclerView rvCoffee;
    private CoffeeAdapter coffeeAdapter;
    private ArrayList<Coffee> coffees;
    private EditText etWeigh;
    private String jenis;
    private Coffee mData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_resource);

        rvCoffee = findViewById(R.id.rv_list);
        etWeigh = findViewById(R.id.editTextTextPersonName);
        mData = new Coffee();
        Button btnAdd = findViewById(R.id.button);
        btnAdd.setOnClickListener(this);

        rvCoffee.setHasFixedSize(true);
        rvCoffee.setLayoutManager(new LinearLayoutManager(this));
        rvCoffee.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        coffeeAdapter = new CoffeeAdapter();
        coffees = new ArrayList<>();
        coffeeAdapter.setCoffees(coffees);
        rvCoffee.setAdapter(coffeeAdapter);

        final Spinner spinner = findViewById(R.id.spinner);
        spinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                jenis = spinner.getSelectedItem().toString();
            }
        });

        if (jenis == null) {
            jenis = String.valueOf(spinner.getSelectedItem());
        }
        new LoadNotesAsync(this, this).execute();
    }

    @Override
    public void preExecute() {

    }

    @Override
    public void postExecute(ArrayList<Coffee> coffees) {
        if (coffees.size() > 0) {
            coffeeAdapter.setCoffees(coffees);
        } else {
            coffeeAdapter.setCoffees(new ArrayList<Coffee>());
            showSnackbarMessage("Tidak ada data saat ini");
        }
    }

    private void showSnackbarMessage(String message) {
        Snackbar.make(rvCoffee, message, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.button) {
            String w = etWeigh.getText().toString().trim();
            String nm = jenis;
            if (TextUtils.isEmpty(w)) {
                etWeigh.setError("Field can not be blank");
                return;
            }

            ContentValues values = new ContentValues();
            values.put(DatabaseContract.CategoryColumns.COFFEE_NAME, nm);
            values.put(DatabaseContract.CategoryColumns.DRY_WEIGHT, w);

            getContentResolver().insert(CONTENT_URI, values);
            Toast.makeText(ListResourceActivity.this, "Satu item berhasil disimpan", Toast.LENGTH_SHORT).show();
        }
    }

    public static class LoadNotesAsync extends AsyncTask<Void, Void, ArrayList<Coffee>> {
        private final WeakReference<Context> weakContext;
        private final WeakReference<LoadCoffeeCallback> weakCallback;

        LoadNotesAsync(Context context, LoadCoffeeCallback callback) {
            weakContext = new WeakReference<>(context);
            weakCallback = new WeakReference<>(callback);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            weakCallback.get().preExecute();
        }

        @Override
        protected ArrayList<Coffee> doInBackground(Void... voids) {
            Context context = weakContext.get();
            Cursor dataCursor = context.getContentResolver().query(CONTENT_URI, null, null, null, null);
            return MappingHelper.mapCursorToArrayList(dataCursor);
        }

        @Override
        protected void onPostExecute(ArrayList<Coffee> coffees) {
            super.onPostExecute(coffees);
            weakCallback.get().postExecute(coffees);
        }
    }
}