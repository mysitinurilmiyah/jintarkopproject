package com.example.jintarkop;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;


/**
 * A simple {@link Fragment} subclass.
 */
public class MonitorFragment extends Fragment {
    private static final String TAG = "eaak";
    private String[] jenis_kopi;



    public MonitorFragment () {
        // Required empty public constructor
    }

    TextView data_berat1, data_berat2;
    Button btn_databerat, btKering1, btKering2, btJenisKopi;
    EditText kering1, kering2;
    DatabaseReference reffdb;
    ProgressBar progressBar, progressBar2;


    @Override
    public View onCreateView ( LayoutInflater inflater , ViewGroup container ,
                               Bundle savedInstanceState ) {
        // Inflate the layout for this fragment
        View v = inflater.inflate ( R.layout.fragment_monitor , container , false );
        this.jenis_kopi = new String[] {
                "1", "2", "3", "4", "5"
        };
        Spinner s = v.findViewById(R.id.spinnerJenis);
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        s.setAdapter(spinnerAdapter);
        spinnerAdapter.add ( "tes" );
        spinnerAdapter.add ( "tes" );
        spinnerAdapter.add ( "tes" );
        spinnerAdapter.notifyDataSetChanged();
        data_berat1 = v.findViewById ( R.id.berat1 );
        data_berat2 = v.findViewById ( R.id.berat2 );
        btn_databerat = v.findViewById ( R.id.btn_timbang );
        kering1= v.findViewById ( R.id.kering1 );
        kering2= v.findViewById ( R.id.kering );
        btKering1 = v.findViewById ( R.id.btn_kering );
        btKering2 = v.findViewById ( R.id.btn_kering2 );
        btJenisKopi = v.findViewById ( R.id.btn_kering3 );
        progressBar = v.findViewById ( R.id.progressBar );
        progressBar2 = v.findViewById ( R.id.progressBar2 );
        //final String toast = "Berat Kopi Berhasil Ditimbang";

        //Toast.makeText ( getContext (),toast,Toast.LENGTH_LONG ).show ();
        reffdb= FirebaseDatabase.getInstance ().getReference ();
        reffdb.child ( "Pengering1" ).addValueEventListener ( new ValueEventListener () {
            @Override
            public void onDataChange ( @NonNull DataSnapshot dataSnapshot ) {
                if (dataSnapshot.hasChild ( "Timbangan1" )){
                    String Timbangan1=dataSnapshot.child ( "Timbangan1" ).getValue ().toString ();
                    String Timbangan2=dataSnapshot.child ( "Timbangan2" ).getValue ().toString ();
                    String berat1=dataSnapshot.child ( "beratKopiKering1" ).getValue ().toString ();
                    String berat2=dataSnapshot.child ( "beratKopiKering2" ).getValue ().toString ();
                    data_berat1.setText ( Timbangan1 );
                    data_berat2.setText ( Timbangan2 );
                    Log.d ( "berat ==", berat1 );
                    Log.d ( "timbangan1 ==", Timbangan1);

                    int a = Integer.parseInt ( Timbangan1 );
                    int b = Integer.parseInt ( berat1 );
                    int hasil;
                    if ( a > b){
                        hasil = getPersenAir(a,b);
                        progressBar.setMax ( 100 );
                        progressBar.setProgress ( hasil );
                    } else {
                        progressBar.setMax ( 100 );
                        progressBar.setProgress ( 100 );
                    }


                    int as = Integer.parseInt ( Timbangan2 );
                    int bs = Integer.parseInt ( berat2 );
                    int hasils;
                    if ( as > bs){
                        hasils = getPersenAir(as,bs);
                        progressBar2.setMax ( 100 );
                        progressBar2.setProgress ( hasils );
                    } else {
                        progressBar2.setMax ( 100 );
                        progressBar2.setProgress ( 100 );
                    }

                    if (a < b){
                        Log.d ( "notif1 ==", berat1 );

                        MyFirebaseService.showNotification(getActivity (), "Pemberitahuan", "Kopi Kering");
                    }

                    if (as < bs){
                        Log.d ( "notif2 ==", berat2 );

//                        Log.d ( "berat2 ==", Integer.parseInt(berat2));
//                        Log.d ( "timbangan2 ==",  Integer.parseInt(Timbangan2));

                        MyFirebaseService.showNotification(getActivity (), "Pemberitahuan", "Kopi Kering");
                    }
                }
            }

            @Override
            public void onCancelled ( @NonNull DatabaseError databaseError ) {

            }
        } );

        btn_databerat.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick ( View v ) {

            }
        } );

        btKering1.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick ( View v ) {
                if (!TextUtils.isEmpty ( kering1.getText ().toString () )){
                    reffdb = FirebaseDatabase.getInstance ().getReference ().child ( "Pengering1" );
                    reffdb.child ( "beratKopiKering1" ).setValue ( kering1.getText ().toString () );
                } else {
                    kering1.setError ( "Harus diisi" );
                    Toast.makeText ( getActivity () , "Berat Kering1 Kosong" , Toast.LENGTH_SHORT ).show ();
                }

            }
        } );

        btKering2.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick ( View v ) {
                if (!TextUtils.isEmpty ( kering2.getText ().toString () )){
                    reffdb = FirebaseDatabase.getInstance ().getReference ().child ( "Pengering1" );
                    reffdb.child ( "beratKopiKering2" ).setValue ( kering2.getText ().toString () );

                }else {
                    kering2.setError ( "Harus diisi" );
                    Toast.makeText ( getActivity () , "Berat Kering2 Kosong" , Toast.LENGTH_SHORT ).show ();
                }

            }
        } );

        btJenisKopi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(),ListResourceActivity.class));
            }
        });


        return v;


    }

    private int getPersenAir(int semuaData, int dataSekarang){
        float a = (float) semuaData;
        float b = (float) dataSekarang;
        float hasil = b/a * 100;
        return (int) hasil;
    }

}
