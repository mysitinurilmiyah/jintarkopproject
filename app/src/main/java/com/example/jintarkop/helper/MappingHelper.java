package com.example.jintarkop.helper;

import android.database.Cursor;

import com.example.jintarkop.db.DatabaseContract;
import com.example.jintarkop.pjo.Coffee;

import java.util.ArrayList;

public class MappingHelper {
    public static ArrayList<Coffee> mapCursorToArrayList(Cursor coffeeCursor) {
        ArrayList<Coffee> list = new ArrayList<>();
        while (coffeeCursor.moveToNext()) {
            int id = coffeeCursor.getInt(coffeeCursor.getColumnIndexOrThrow(DatabaseContract.CategoryColumns._ID));
            String name = coffeeCursor.getString(coffeeCursor.getColumnIndexOrThrow(DatabaseContract.CategoryColumns.COFFEE_NAME));
            String weight = coffeeCursor.getString(coffeeCursor.getColumnIndexOrThrow(DatabaseContract.CategoryColumns.DRY_WEIGHT));
            list.add(new Coffee(id, name, weight));
        }
        return list;
    }

    public static Coffee mapCursorToObject(Cursor coffeeCursor) {
        coffeeCursor.moveToFirst();
        int id = coffeeCursor.getInt(coffeeCursor.getColumnIndexOrThrow(DatabaseContract.CategoryColumns._ID));
        String name = coffeeCursor.getString(coffeeCursor.getColumnIndexOrThrow(DatabaseContract.CategoryColumns.COFFEE_NAME));
        String weight = coffeeCursor.getString(coffeeCursor.getColumnIndexOrThrow(DatabaseContract.CategoryColumns.DRY_WEIGHT));
        return new Coffee(id, name,weight);
    }
}
