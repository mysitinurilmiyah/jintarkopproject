package com.example.jintarkop;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    ImageButton button_aboutus;
//    ImageButton button_set;

    public HomeFragment () {
        // Required empty public constructor
    }


    @Override
    public View onCreateView ( LayoutInflater inflater , ViewGroup container ,
                               Bundle savedInstanceState ) {

        View v = inflater.inflate ( R.layout.fragment_home , container , false );

////        button set monitoring
//        button_set = v.findViewById ( R.id.btn_setmot );
//        button_set.setOnClickListener ( new View.OnClickListener () {
//            @Override
//            public void onClick ( View v ) {
//                Intent in = new Intent ( getActivity (), SetMonitoring.class );
//                in.putExtra ( "some", "Berat Kering Pada Set Monitoring" );
//                startActivity ( in );
//            }
//        } );

        //button about us
        button_aboutus = v.findViewById ( R.id.btn_aboutus );
        button_aboutus.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick ( View v ) {
               Intent in = new Intent ( getActivity (), AboutUs.class );
               in.putExtra ( "some", "Aplikasi Jintarkop" );
               startActivity ( in );

            }
        } );

        return v;

    }

}
