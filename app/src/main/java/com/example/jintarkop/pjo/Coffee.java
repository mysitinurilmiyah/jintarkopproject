package com.example.jintarkop.pjo;

public class Coffee {
    private int id;
    private String name;
    private String weight;

    public Coffee() {
    }

    public Coffee(int id, String name, String weight) {
        this.id = id;
        this.name = name;
        this.weight = weight;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }
}
